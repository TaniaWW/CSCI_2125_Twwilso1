/**
 * Homework 1
 * CSCI 2125
 * 6/18/2018
 * @author Tania Wilson
 */
import java.util.Scanner;
import java.util.ArrayList;
import java.util.ArrayDeque;

public class RunClock{
    
    //function checks if queue is sorted in numerical order
    public static boolean isQueueSorted(ArrayDeque<Integer> queue){
        Integer[] array = queue.toArray(new Integer[queue.size()]);
        for (int i = 0; i < queue.size(); i++){
            if (array[i] != i+1){
                return false;
            }
        }
        return true;
    }
   
    public static void main(String[] args)
    {
        int i = 0;
        //Array of number of balls going to be put in a queue
        ArrayList<Integer> numBalls = new ArrayList<>();

        //Takes user input and stores it until the user inputs '0'
        Scanner ballCount = new Scanner(System.in);
        while(ballCount.hasNextInt()){
            int userInput = ballCount.nextInt();
            if(userInput == 0){
                break;
            }
            else{
                numBalls.add(userInput);
                i++;
            }
        }
        
        //while there are user inputs (clocks to run), the program will run
        for(int j = 0; j < i; j++){
            
            //Creating queue
            int queueSize = numBalls.get(j);
            ArrayDeque<Integer> clockQueue = new ArrayDeque<>(queueSize);
            
            //inserting balls from 1 to however many the user wanted, into the clock queue
            for(int k = 1; k <= queueSize; k++){
                clockQueue.add(k);
            }

            //initializing clock tracks/rails
            ArrayDeque<Integer> minTrack = new ArrayDeque<>();
            ArrayDeque<Integer> fivMinTrack = new ArrayDeque<>();
            ArrayDeque<Integer> hourTrack = new ArrayDeque<>();

            /**
            *Running clock/clocks. I set the capacity of each track to more than
            *it could handle to simulate a "last" ball coming onto each track
            * before it tips
            */
            int twelveHours = 0;
            do{
                for(int hourCount = 1; hourCount <= 12; hourCount++){
                    
                    for(int fivCount = 1; fivCount <= 12; fivCount++){
                        
                        //add balls to the first minutes stack
                        for(int minCount = 1; minCount <= 5; minCount++){
                            minTrack.push(clockQueue.poll());
                        }
                        
                        //last ball of the minute track is added to the 5 min stack
                        fivMinTrack.push(minTrack.pop());

                        //rest of the balls are re-queued
                        while(!minTrack.isEmpty()){
                            clockQueue.add(minTrack.pop());
                        }
                    }
                    //Last 5 min ball is added to hour stack
                    hourTrack.push(fivMinTrack.pop());

                    //rest of the balls are re-queued
                    while(!fivMinTrack.isEmpty()){
                        clockQueue.add(fivMinTrack.pop());
                    }
                }
                //Moving the last ball added to the other end of the hour stack
                hourTrack.offer(hourTrack.pop());
                //requeuing the hours
                while(!hourTrack.isEmpty()){
                    clockQueue.add(hourTrack.pop());
                }
                //keeps track of every 12 hours passed
                twelveHours++;
            }while (isQueueSorted(clockQueue) == false);
            
            //final statement
            System.out.println(queueSize + " balls cycle after "+ twelveHours/2 + " days.");
        }
    }
}
